package com.mercadolibre.ui

import android.app.Application
import com.mercadolibre.ki.sdAplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext

class AppMercado: Application() {
    override fun onCreate() {
        super.onCreate()

        GlobalContext.startKoin {
            androidLogger()
            androidContext(this@AppMercado)
            modules(
                listOf(
                    sdAplication
                )
            )
        }

    }
}