package com.mercadolibre.ui.Screem


import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import coil.compose.AsyncImagePainter
import coil.compose.SubcomposeAsyncImage
import coil.compose.SubcomposeAsyncImageContent
import com.google.gson.Gson
import com.google.gson.GsonBuilder

import com.mercadolibre.Response.Item
import java.net.URLEncoder
import java.nio.charset.StandardCharsets


@Composable
fun Producto(item: Item, navController: NavHostController){

    val gson: Gson = GsonBuilder().create()
    val toJson = gson.toJson(item)
    var encode = URLEncoder.encode(toJson, StandardCharsets.UTF_8.toString())
    Row(Modifier.fillMaxWidth().padding(16.dp).clickable { navController.navigate("detalle/${encode}") }) {
        SubcomposeAsyncImage(
            model = item.thumbnail,
            contentDescription = "",
            modifier = Modifier.width(128.dp).height(128.dp)
        ) {
            val state = painter.state
            if (state is AsyncImagePainter.State.Loading || state is AsyncImagePainter.State.Error) {
                CircularProgressIndicator()
            } else {
                SubcomposeAsyncImageContent()
            }
        }


        Column(Modifier.padding(start = 16.dp, end = 16.dp)){
            Text(text = item.title,
            color = Color.Black)
            Text(text = "${item.price}",
                color = Color.Black,
                fontWeight = FontWeight.ExtraBold
            )
            val installments = item.installments
            var completo = "";
            if (installments?.rate == 0.0) {
                Text(
                    text = "${item.installments?.quantity} x ${item.installments?.amount}  MSI",
                    fontWeight = FontWeight.ExtraBold,
                    color = Color.Green,
                    fontSize = 10.sp
                )

            }else {
                Text(
                    text = "${item.installments?.quantity} x ${item.installments?.amount} ",
                    fontWeight = FontWeight.ExtraBold,
                    fontSize = 10.sp,
                    color = Color.Black
                )

            }



        }


    }



}

