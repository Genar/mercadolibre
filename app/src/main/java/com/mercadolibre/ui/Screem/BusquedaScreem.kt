package com.mercadolibre.ui.Screem

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.getValue

import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll

import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TextFieldDefaults

import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign

import com.mercadolibre.Model.BusquedaModel
import com.mercadolibre.R
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController

@Composable
fun busqueda(navController: NavHostController,busquedaModel : BusquedaModel = viewModel()){

    val uiState by busquedaModel.uiState.collectAsState()
    Box(Modifier.fillMaxSize().background(Color.White)){
        Column(Modifier.fillMaxSize()) {
            Toolbar()
            EditTexBusqueda(busquedaModel)
            if (uiState.mostrarError) {
                Error(busquedaModel)
            }
            if (uiState.mostrarProgress) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally){
                    CircularProgressIndicator(
                        modifier = Modifier.width(64.dp),
                        color = Color.Blue,
                        strokeWidth = 10.dp
                    )
                    Text(text = stringResource(id = R.string.consultando),
                        Modifier.padding(top = 30.dp),
                    color = Color.Blue)
                }

            }
            if (uiState.pruductos.size > 0){
                Box(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .padding(32.dp)
                ) {
                    Column {
                        val pruductos = uiState.pruductos;
                        pruductos.forEach { producto ->
                            Column(Modifier.background(Color.White)) {
                                Producto(producto, navController)
                                Divider(modifier = Modifier.width(1.dp), color = Color.Black)
                            }

                        }
                    }
                }
            }
        }


    }
}


// funcion para mostar un mensaje si no tiene conexion
@Composable
fun Error(busquedaModel: BusquedaModel){
    val uiState by busquedaModel.uiState.collectAsState()
   Column(
       Modifier
           .fillMaxWidth()
           .fillMaxHeight(),
   verticalArrangement = Arrangement.Center)
   {

       Text(text = uiState.mensajeError,
           modifier = Modifier.fillMaxWidth(),
          color= Color.Red,
           fontSize = 24.sp,
       textAlign = TextAlign.Center)


   }
}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Toolbar(){
    TopAppBar(
        title = { Text(text = stringResource(R.string.app_name), color = colorResource(id = R.color.black))},
        colors = TopAppBarDefaults.mediumTopAppBarColors(colorResource(id =R.color.yelow ))
    )
}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditTexBusqueda(busquedaModel : BusquedaModel){


    val onUsernameChange = remember { mutableStateOf("") }
    Row(Modifier.fillMaxWidth().background(colorResource(id =R.color.yelow )),
       horizontalArrangement = Arrangement.SpaceAround){
        TextField(
            value = onUsernameChange.value,
            onValueChange = { onUsernameChange.value=  it },
            Modifier.weight(1f),
            label = { Text("Producto a buscar", color = Color.White) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text,imeAction = ImeAction.Done),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = Color.Transparent, textColor = Color.Black)

        )
        Boton( {  busquedaModel.getProdcutos(onUsernameChange.value)},
            icono = painterResource(id = R.drawable.ic_buscar),
            stringResource(id = R.string.buscar))
    }

}


@Composable
fun Boton(click : () -> Unit, icono: Painter, text: String){
    Button(onClick = { click() },
        shape = CircleShape,
        border = BorderStroke(1.dp, colorResource(id = R.color.white)),
        colors = ButtonDefaults.outlinedButtonColors(colorResource(id = R.color.blue))
    ) {
        Image(
            painter = icono,
            contentDescription = "guardar",
            modifier = Modifier.size(size = 28.dp)
        )

    }
}
