package com.mercadolibre.ui.Screem

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImagePainter
import coil.compose.SubcomposeAsyncImage
import coil.compose.SubcomposeAsyncImageContent
import com.mercadolibre.R
import com.mercadolibre.Response.Item

@Composable
fun DetalleScreem(item: Item) {

    Column(Modifier.fillMaxSize().background(Color.White)) {
        Toolbar()
        Box( Modifier.fillMaxSize().verticalScroll(rememberScrollState())){
            Detalle(item)
        }

    }
}
@Composable
fun Detalle(item: Item){
    Column(
        Modifier
            .fillMaxWidth()
            .padding(16.dp)) {
        Text(text = item.title,
            fontSize = 18.sp,
            color = Color.Black
        )
        SubcomposeAsyncImage(
            model = item?.thumbnail,
            contentDescription = "",
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
        ) {
            val state = painter.state
            if (state is AsyncImagePainter.State.Loading || state is AsyncImagePainter.State.Error) {
                CircularProgressIndicator()
            } else {
                SubcomposeAsyncImageContent()
            }
        }


        Column(Modifier.padding(start = 16.dp, end = 16.dp)){

            Text(text = "${item.price}",
                fontWeight = FontWeight.ExtraBold,
                fontSize = 16.sp,
                color = Color.Black
            )

            Text(text = "${item.address?.city_name}, ${item.address?.state_name}",
               fontSize = 10.sp,
                color = Color.Black
            )

            Text(text = stringResource(id = R.string.caracteristicas),
                Modifier.padding(top = 16.dp, bottom = 16.dp),
                fontSize = 14.sp,
                color = Color.Black
            )
            Column(Modifier.fillMaxWidth().padding(start = 10.dp, end = 10.dp).
            border(2.dp, Color.Black, RoundedCornerShape(10.dp))) {

                item.attributes?.forEach { atri ->
                    Row(Modifier.fillMaxWidth().padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceEvenly) {

                        Text(
                            text = "${atri.name}",
                            Modifier.weight(1f),
                            fontSize = 14.sp,
                            textAlign = TextAlign.Left,
                            color = Color.Black
                        )
                        Text(
                            text = "${atri.value_name}",
                            Modifier.weight(1f),
                            fontSize = 14.sp,
                            textAlign = TextAlign.Left,
                            color = Color.Black
                        )
                    }

                }

            }




        }


    }
}