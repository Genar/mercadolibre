package com.mercadolibre.ui.sate

import com.mercadolibre.Response.Item
import com.mercadolibre.Response.MonedaResponse

data class UiState (
    var mostrarError : Boolean = false,
    var mostrarProgress : Boolean = false,
    var mensajeError : String = "",
    var pruductos:List<Item> = mutableListOf(),
    var monedas:List<MonedaResponse> = mutableListOf(),


)