package com.mercadolibre.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.mercadolibre.Response.Item
import com.mercadolibre.Response.MonedaResponse
import java.text.DecimalFormat

class Util(var context: Context) {
    // funcion: Verificar la conexión a internet
    // Parametros :
    // retorna : el estatus de la conexion
    fun conexionInternet(): Boolean {
        var result = false
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }

        return result
    }
    // funcion: Colocar el simbolo dependiente el valor de la moneda
    // Parametros : Lista de producto y lista de monedas
    // retorna :

    fun setFormato(productos: List<Item>?, monedas: List<MonedaResponse>){
        if(productos != null) {
            Log.d("TAGML", "logintud de productos" + productos.size)
            for (producto in productos) {
                val formatoMoneda = getFormatoMoneda(producto.price, producto.currency_id, monedas)
                producto.price = formatoMoneda
            }
        }
    }
    // funciones para obtener el formato del a la moneda y el simbolo
    // Parametros : precio, codigo del producto, la lista de monedas
    // retorna : retorna el precio con su simbolo y formato
    fun getFormatoMoneda(precio: String, codiogo :String, monedas : List<MonedaResponse>) : String{
        val df = DecimalFormat("###,###,###.##")
        var valor =precio;
        var roundoff= precio.toDouble()
        for (moneda in monedas){
            Log.d("TAGML", "moneda " + moneda.id +  " codigo " + codiogo)
            if (moneda.id.equals(codiogo)){
                var roundoff= precio.toDouble()
                valor = df.format(roundoff);
                return  "${moneda.symbol} ${valor}"
            }
        }
       return valor
    }
}