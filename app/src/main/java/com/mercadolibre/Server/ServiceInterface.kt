package com.mercadolibre.Server


import com.mercadolibre.Response.ItemResponse
import com.mercadolibre.Response.MonedaResponse
import retrofit2.Call
import retrofit2.Response

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ServiceInterface {

    @GET("/sites/{siteId}/search")
    suspend fun getProductos(  @Path("siteId") siteId: String,
                         @Query("q") busqueda: String): Response<ItemResponse>
    @GET("currencies")
    suspend fun getMonedas(): Response<List<MonedaResponse>>

}