package com.mercadolibre.Server

import com.google.gson.GsonBuilder
import com.mercadolibre.util.Constant
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    val webService: ServiceInterface by lazy {
        Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ServiceInterface::class.java)
    }
}