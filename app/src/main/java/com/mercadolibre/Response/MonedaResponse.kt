package com.mercadolibre.Response

data class MonedaResponse (
    var id : String,
    var symbol : String,
    var description : String,
    var decimal_places : Int,
    )
