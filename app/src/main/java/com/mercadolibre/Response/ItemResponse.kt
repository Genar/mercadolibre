package com.mercadolibre.Response

class ItemResponse() {

    var site_id:String ?= null
    var country_default_time_zone:String ?= null
    var query:String ?= null
    var paging: Paging ?= null
    var results : List<Item> ?= null

}
class Paging{
  var total : Int ?= 0
  var primary_results : Int ?= 0
  var offset : Int ?= 0
  var limit : Int ?= 0
}

class Item {
    var id: String ?= null
    var title : String  = ""
    var condition : String  ?= null
    var thumbnail_id : String  ?= null
    var catalog_product_id : String  ?= null
    var listing_type_id : String  ?= null
    var permalink : String  ?= null
    var buying_mode : String  ?= null
    var site_id : String  ?= null
    var category_id : String  ?= null
    var domain_id : String  ?= null
    var variation_id : String  ?= null
    var thumbnail : String  ?= null
    var currency_id : String  = ""
    var order_backend : Int  ?= null
    var price : String  = "";
    var seller : Seller  ?= null
    var address : Address  ?= null
    var attributes : List<Attributes>  ?= null
    var installments : Installments ? = null
}
class  Seller {
   var id:  String ?= null
   var nickname:  String ?= null
}
class  Attributes{
  var id :  String ? = null
  var name :  String ? = null
  var value_id :  String ? = null
  var value_name :  String ? = null
}
class Address {
    var state_id : String ?= null
    var state_name : String ?= null
    var city_name : String ?= null
}
class  Installments{
    var quantity : Int =  0
    var amount : Double =  0.0
    var rate : Double =  0.0
    var currency_id : String =  ""

}
