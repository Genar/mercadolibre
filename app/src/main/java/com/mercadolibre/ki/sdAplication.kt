package com.mercadolibre.ki

import android.content.Context
import com.mercadolibre.ui.sate.UiState
import com.mercadolibre.util.Util
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val  sdAplication  = module{
    single {getContext(androidContext()) }
    single {getUiState() }
}
fun getContext(androidContext: Context): Util{
    return Util(androidContext)
}
fun getUiState() : UiState{
    return UiState()
}