package com.mercadolibre.Model

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.mercadolibre.R
import com.mercadolibre.Response.Item
import com.mercadolibre.Response.MonedaResponse
import com.mercadolibre.Server.RetrofitClient
import com.mercadolibre.ki.getUiState
import com.mercadolibre.ui.sate.UiState
import com.mercadolibre.util.Constant
import com.mercadolibre.util.Util
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent

class BusquedaModel(): ViewModel() {

    val ui : UiState by KoinJavaComponent.inject(UiState::class.java)
    private val _uiState = MutableStateFlow(getUiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()

    init {
        getMonedas()
    }
    // funcion: Busqueda de productos de acuerdo la busqueda
    // Parametros : Busqueda de tipo string
    // retorna :
    fun getProdcutos(busqueda : String) {
        val util : Util by KoinJavaComponent.inject(Util::class.java)
        updateUiProducto(mutableListOf())

        if (util.conexionInternet()) {

            if (!busqueda.equals("")) {
                updateUi(false,    "", true)
                viewModelScope.launch(Dispatchers.IO) {
                    Log.d("TAGML", "Buscando productos ")
                    val response =
                        RetrofitClient.webService.getProductos("MLM", busqueda)
                    withContext(Dispatchers.Main) {
                        if (response.code() == Constant.exito) {
                            if (response.body() != null) {
                                updateUi(false,    "", false)
                                _uiState.value.mostrarError = false;
                                Log.d("TAGML", "" + response.body()?.results?.size)
                                util.setFormato(response.body()?.results, _uiState.value.monedas)
                                updateUiProducto(response.body()!!.results!!)
                            } else {
                                updateUi(true,    util.context.getString(R.string.error_servicio), false)
                            }
                        } else {
                            updateUi(true,    util.context.getString(R.string.error_servicio), false)
                        }


                    }
                }
            }
       }else {
            updateUi(true,  util.context.getString(R.string.sin_internet) , false)
        }
    }


    // funcion:  Obtener las monedas disponibles en servicio
    // Parametros :
    // retorna :
    fun getMonedas(){
        val util : Util by KoinJavaComponent.inject(Util::class.java)
        viewModelScope.launch(Dispatchers.IO) {
            val response =
                RetrofitClient.webService.getMonedas()
            withContext(Dispatchers.Main) {
                if (response.code() == Constant.exito) {
                    if (response.body() != null) {
                        _uiState.value.mostrarError = false;
                        Log.d("logitud", "mondedas " + response.body()!!.size)
                        updateUiMonedas(response.body()!!)
                    } else {
                        updateUi(true,    util.context.getString(R.string.error_servicio), false)
                    }
                } else {
                    updateUi(true,    util.context.getString(R.string.error_servicio), false)
                }


            }
        }

    }

    // funcion: Actualizar la ui, mostrar y cultar el error en view
    // Parametros :  mensaje error,  mostrarProgress y bandera mostrarError para motrar elementos en la view
    // retorna :
    fun updateUi(mostrarError : Boolean, mensajeError : String, mostrarProgress: Boolean){
        _uiState.update { currentState ->
            currentState.copy(
                mostrarError =  mostrarError,
                mensajeError = mensajeError,
                mostrarProgress = mostrarProgress
            )
        }
    }

    // funcion: Actualizar de la lista de productos
    // Parametros :  recibe la lista de productos
    // retorna :
    fun updateUiProducto(list: List<Item>){
        _uiState.update { currentState ->
            currentState.copy(
                pruductos = list
            )
        }
    }
    // funcion: Actualizar de la lista de monedas
    // Parametros :  recibe la lista de monedas
    // retorna :
    fun updateUiMonedas(list: List<MonedaResponse>){
        _uiState.update { currentState ->
            currentState.copy(
                monedas = list
            )
        }
    }

}