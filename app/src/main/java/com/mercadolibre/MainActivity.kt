package com.mercadolibre


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mercadolibre.ui.theme.MercadoLibreTheme
import com.mercadolibre.ui.Screem.DetalleScreem
import com.mercadolibre.ui.Screem.busqueda
import java.net.URLDecoder
import java.nio.charset.StandardCharsets

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MercadoLibreTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navControler = rememberNavController()

                    NavHost(navController = navControler, startDestination = "busqueda" ){
                        composable("busqueda"){
                            busqueda(navControler)
                        }
                        composable("detalle/{id}", arguments = listOf(navArgument("id"){type = NavType.StringType})){
                            backstack->
                            val gson: Gson = GsonBuilder().create()
                            /* se extrae el object json de la ruta */
                            val userJson = backstack.arguments?.getString("id")
                            // convierte json string a  el  Objeto Item data
                            var encode = URLDecoder.decode(userJson, StandardCharsets.UTF_8.toString())
                            val item = gson.fromJson(encode, com.mercadolibre.Response.Item::class.java)


                            DetalleScreem(item!!)
                        }

                    }


                }
            }
        }
    }
}

